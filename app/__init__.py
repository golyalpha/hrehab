import logging

from time import sleep

from .cam import ThreadedCamera
from .process import get_coords

def main(level=logging.INFO, feed=False):
    logging.basicConfig(level=level)
    logging.info("Root logger set up.")

    cam = ThreadedCamera(feed=feed)
   
    try:
        while True:
            coords = get_coords(cam.grab_frame())
            logging.debug(f"Got coords: {coords}")
    except KeyboardInterrupt:
        pass
    finally:
        cam.close()
