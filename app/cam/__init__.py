"""
Module containing classes and functions related to camera operation
"""

import threading
import time
import logging

import cv2

class ThreadedCamera:
    """
    Thraded camera object.
    Continously reads frames from camera and puts them in the buffer.
    """
    def __init__(self, camid: int=0, feed: bool=False):
        self.buffer: list = []
        self.stop: bool = False
        self.feed = feed

        self.cam = cv2.VideoCapture(camid)
        self.thread: threading.Thread = threading.Thread(target=self._read_thread)
        self.thread.start()
        self.logger: logging.Logger = logging.getLogger(f"ThreadedCamera.{camid}")
        self.start_time: float = time.process_time()
        self.logger.info("Camera ready!")

    def close(self):
        """
        Tells the reader thread to stop and waits for it to stop.
        """
        self.logger.info("Reader stop requested.")
        self.stop = True
        self.logger.info("Waiting for reader to stop.")
        self.thread.join()
        self.logger.info("Reader stopped.")

    def grab_frame(self):
        """
        Grab the latest frame from camera.
        Flushes the buffer and counts frames.
        """
        while not len(self.buffer):
            pass
        img = self.buffer[-1]
        self.buffer = []
        return img

    def _read_thread(self):
        while not self.stop:
            _, img = self.cam.read()
            self.buffer.append(img)
            if self.feed:
                cv2.imshow("Camera", self.buffer[-1])
                cv2.waitKey(1)
