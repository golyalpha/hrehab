import logging

import cv2
import numpy as np

logger:logging.Logger = logging.getLogger("Processing")

params = cv2.SimpleBlobDetector_Params()
params.minThreshold = 200
params.maxThreshold = 255
params.filterByArea = False
params.filterByCircularity = False
params.filterByColor = False
params.filterByConvexity = False
params.filterByInertia = False
detector = cv2.SimpleBlobDetector_create(params)

def get_coords(img):
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    reds = [
        np.array([0,  150,  50]),
        np.array([10, 255, 255])
    ]
    kernel = np.ones((4,4), np.uint8)
    denoised = cv2.morphologyEx(
        cv2.inRange(hsv, reds[0], reds[1]),
        cv2.MORPH_OPEN,
        kernel,
        iterations=2
    )
    mask = cv2.dilate(
        denoised,
        kernel,
        iterations=4
    )
    cv2.imshow("Image", img)
    cv2.imshow("Mask", mask)
    cv2.waitKey(1)

    
    keypoints = detector.detect(mask)
    try:
        logger.debug(f"{keypoints[0].pt}")
        cv2.imshow(
            "Selection",
            cv2.drawKeypoints(mask, [keypoints[0]], np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
        )
        return keypoints[0].pt
    except IndexError:
        return -1, -1


def get_corners(img):
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    corner_tags = [
        (
            np.array([0,  100, 100]),
            np.array([10, 255, 255])
        ),
        (
            np.array([85, 100, 100]),
            np.array([95, 255, 255])
        ),
        (
            np.array([175, 100, 100]),
            np.array([185, 255, 255])
        ),
        (
            np.array([265, 100, 100]),
            np.array([275, 255, 255])
        )
    ]
    corner_coords = []
    for tag in corner_tags:
        mask = cv2.inRange(hsv, tag[0], tag[1])
        keypoints = detector.detect(mask)
        corner_coords.append(keypoints[0].pt)
    return corner_coords

